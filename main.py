import math


def main():
    b = 4
    a = int(input("Введите a: "))
    x = int(input("Введите x: "))
    g = (10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2)) / (15 * a ** 2 + 49 * a * x + 24 * x ** 2)
    f = math.tan(math.radians(5 * a ** 2 + 34 * a * x + 45 * x ** 2))

    arcsin_param = math.radians(7 * a ** 2 - a * x - 8 * x ** 2)
    if 1 > arcsin_param > -1:
        y = -1 * math.asin(arcsin_param)
    else:
        y = 'Ошибка'

    print(f'Дано: a = {a}; x = {x}')
    print(f'Ответ: G = {g}; F = {f}; Y = {y}')
    input("Нажмите Enter для завершения")


main()
